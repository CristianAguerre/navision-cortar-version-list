﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

/**
 * Codificación del fichero de Salida en UTF-8
 */
namespace TruncarListaVersionesObjetosNAV
{
    public partial class Form1 : Form
    {
        public String pathReadFile { get; set; }
        public String pathSaveFile { get; set; }
        public String keyWord = "Version List=";
        public int sizeVersionList;

        //Globals to Write
        FileStream fileStreamWrite = null;
        StreamWriter streamWriter = null;

        public Form1()
        {
            InitializeComponent();
        }

        private void destinoToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void acercaDeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("@autores: Francisco José Blanco - Cristian Aguerre Clavel");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("Start Process");
            initFileToWrite(textBoxFicheroDestino.Text);//TODO Refactorizar para utilizar un FileChooser
            loadVarPath();
            sizeVersionList = getNumberOfCharacter();
            readAndSave();
            closeWriterPipe();
        }

        public int getNumberOfCharacter()
        {
            int NumberOfChar = int.Parse(textBoxNumCaracteres.Text);
            return NumberOfChar;
        }

        public void loadVarPath ()
        {
            pathReadFile = textBoxFicheroOrigen.Text;
            pathSaveFile = textBoxFicheroDestino.Text;
        }

        public void readAndSave()
        {
            String line = null;
            //read
            FileStream fileStream = new FileStream(pathReadFile, FileMode.Open);
            StreamReader streamReader = new StreamReader(fileStream, Encoding.UTF8);
            System.Diagnostics.Debug.WriteLine(keyWord);
            while ((line = streamReader.ReadLine()) != null)
            {
                //TODO Refactorizar y mover a un metodo a parte
                if (line.Contains(keyWord))
                {
                    String initialLine = null;
                    String newVersionList = null;
                    String finalLine = null;

                    int indexStart = 0;
                    int indexAux = 0;
                    indexAux = line.IndexOf(keyWord);

                    //IndexAux -> Primera posición del texto encontrado. KeyWord.Lenght -> Tamaño de la cadena "Version List="
                    indexStart = indexAux + keyWord.Length;

                    initialLine = line.Substring(0, indexStart);
                    newVersionList = line.Substring(indexStart, sizeVersionList);
                    finalLine = initialLine + newVersionList;

                    writeSimpleLine(finalLine);
                    //System.Diagnostics.Debug.WriteLine("Inicio version List " + indexStart);
                    //System.Diagnostics.Debug.WriteLine("Nueva Lista de versiones " + newVersionList);
                    //System.Diagnostics.Debug.WriteLine("InitalLine " + initialLine);
                    //System.Diagnostics.Debug.WriteLine("finalLine " + finalLine);
                    //System.Diagnostics.Debug.WriteLine(finalLine);

                } else
                {
                    writeSimpleLine(line);
                    //System.Diagnostics.Debug.WriteLine(line); 
                }
            }
            
            streamReader.Close();
            fileStream.Close();
        }

        public void fileChooserRead()
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            // Filtramos solo ficheros .txt
            openFileDialog1.Filter = "Text Files (.txt)|*.txt|All Files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;

            openFileDialog1.Multiselect = false;
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                textBoxFicheroOrigen.Text = openFileDialog1.FileName;
                System.Diagnostics.Debug.WriteLine("Ruta Cargada: " + pathReadFile);
            }

        }

        public void initFileToWrite(String pathFile)
        {
           fileStreamWrite = new FileStream(pathFile, FileMode.Create);
           streamWriter = new StreamWriter(fileStreamWrite, Encoding.UTF8);
        }

        public void writeSimpleLine (String line)
        {            
            streamWriter.WriteLine(line);
            streamWriter.Flush();          
        }

        public void closeWriterPipe()
        {
            streamWriter.Close();
            fileStreamWrite.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            fileChooserRead();
        }
    }
}
